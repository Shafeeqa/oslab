#include<stdio.h>
#include<stdlib.h>
int n,m;
int dead(int avail[], int alloc[n][m], int req[n][m])
{
	int work[m], check[m];
	for(int i=0;i<m;i++)
	{
		check[i]=avail[i];
	}
	int finish[n];
	for(int i=0;i<n;i++)
	{
		finish[i]=0;
	}
    for(int i=0;i<n;i++)    
        {
            for(int j=0;j<m;j++)
                 {
                   check[j]=check[j]+alloc[i][j];
                 }
         }

	int comp=1;
	int count=n*n;
	int i =0;
	int cond=0;
	while(count>0)
	{
		if(finish[i]==0)
		{
			for(int j=0;j<m;j++)
				if(req[i][j]>work[j])
			{
				cond=1;
				break;
			}
			if(cond==0)
			{
				for(int j=0;j<m;j++)
				{
					work[j]+=alloc[i][j];
				}
				finish[i]=comp++;
			}
		}
		count--;
		i++;
		if(i==n)
			i=0;
		cond=0;
	}
	for(int i=0;i<n;i++)
	{
		if(finish[i]==0)
		{
			cond=1;
			break;
		}
	}
	return cond;
}

int main()
{
	printf("Enter processes and resources \n");
	scanf("%d %d",&n,&m);
	int req[n][m], alloc[n][m], avail[m], need[n][m];

	printf("Enter request \n");
	for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
            scanf("%d", &req[i][j]);
    }
    printf("Enter allocated \n");
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
        {
            scanf("%d", &alloc[i][j]);
        }
    }
    printf("Enter rem avail \n");
    for(int i=0;i<m;i++)
    {
        scanf("%d", &avail[i]);
    }
    int x = dead(avail, alloc, need);
    if(x==0)
        printf("no deadlock detected\n");
    else
        printf("deadlock detected\n"); 
    return 0;
}