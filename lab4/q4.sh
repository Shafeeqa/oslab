if [ "$1" = "-linecount" ]; then
	wc -l "$2"
elif [ "$1" = "-wordcount" ]; then
	wc -w "$2"
elif [ "$1" = "-charcount" ]; then
	wc -c "$2"
fi

