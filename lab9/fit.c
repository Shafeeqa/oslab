#include<stdio.h>
#include<stdlib.h>
struct hole
{
	int no;
	int size;
	int avail;
	int used;
	int p;
}hole;
struct process
{
	int no;
	int size;
	int h;
}process;
int main()
{
	struct hole *holes;
	struct process *processes;
	int nh,np,i,j;
	printf("Enter the number of holes \n");
	scanf("%d",&nh);
	printf("Enter the number of processes \n");
	scanf("%d",&np);
	holes = (struct hole*)calloc(nh,sizeof(struct hole));
	processes = (struct process*)calloc(np,sizeof(struct process));
	printf("enter the size of each hole \n");
	for(i=0;i<nh;i++)
	{
		int x;
		scanf("%d",&x);
		holes[i].size=x;
		holes[i].no=i;
		holes[i].avail=holes[i].size;
		holes[i].used=0;
		holes[i].p=-1;
		printf("No %d \n",&holes[i].no);
	}
	printf("enter the size of each process \n");
	for(i=0;i<np;i++)
	{
		int x;
		scanf("%d",&x);
		processes[i].size=x;
		processes[i].no=i;
		processes[i].h=-1;
	}
	for(i=0;i<np;i++)
	{
		for(j=0;j<nh;j++)
		{
			if(holes[j].avail<processes[i].size )
			{
				holes[j].avail-=processes[i].size;
				holes[j].used=holes[j].size-holes[j].avail;
				holes[j].p=i;
				processes[i].h=j;
				break;
			}
		}
	}
	printf("hole info\n");
	for(i=0;i<nh;i++)
	{
		printf("No %d \n",&holes[i].no);
		printf("Size %d \n",&holes[i].size);
		printf("Used %d \n",&holes[i].used);
		printf("Available %d \n",&holes[i].avail);
		printf("Process%d \n",&holes[i].p);
	}
	printf("process info\n");
	for(i=0;i<np;i++)
	{
		printf("No %d \n",&processes[i].no);
		printf("Size %d \n",&processes[i].size);
		printf("Hole%d \n",&processes[i].h);
	}
	return 0;
}