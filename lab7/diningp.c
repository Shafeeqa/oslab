#include <pthread.h> 
#include <semaphore.h> 
#include <stdio.h> 

#define N 5 
#define think 2 
#define hgr 1 
#define e 0 

int state[N]; 
int phil[N] = { 0, 1, 2, 3, 4 }; 

sem_t mutex; 
sem_t S[N]; 

void test(int n) 
{ 
	if (state[n]==hgr && state[(n+1)%N]!=e && state[(n+4)%N]!=e) 
	{  
		state[n]=e; 
		printf("Philosopher %d is eating\n", n+1); 
		sem_post(&S[n]); 
	} 
} 

void take(int n) 
{ 
	sem_wait(&mutex); 
	state[n] = hgr; 
	printf("Philosopher %d is hungry\n", n + 1); 
	test(n); 
	sem_post(&mutex); 
	sem_wait(&S[n]); 
} 
void put(int n) 
{ 
	sem_wait(&mutex); 
	state[n] = think; 
	printf("Philosopher %d is thinking\n", n + 1); 
	test((n+4)%N); 
	test((n+1)%N); 
	sem_post(&mutex); 
} 

void* philospher(void* num) 
{ 
	int* i=num; 
	take(*i); 
	put(*i); 
} 

int main() 
{ 
	int i; 
	pthread_t pt[N]; 
	sem_init(&mutex, 0, 1); 
	for(i=0;i<N;i++) 
		{
			sem_init(&S[i], 0, 0); 
		}
	for(i=0;i<N;i++)
	{ 
		pthread_create(&pt[i], NULL, philospher, &phil[i]); 
		printf("Philosopher %d is thinking\n", i + 1); 
	} 

	for (i = 0; i < N; i++) 
	{
		pthread_join(pt[i], NULL);
	} 
} 
